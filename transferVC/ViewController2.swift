//
//  ViewController2.swift
//  transferVC
//
//  Created by Admin on 3/4/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

protocol ViewController1Delegate {
    func passData(with data: String)
}

class ViewController2: UIViewController {

    @IBOutlet weak var button2: UIButton!
    
    var delegate: ViewController1Delegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        button2.addTarget(self, action: #selector(showAlert), for: .touchUpInside)
    }
    
    @objc func showAlert(){
        let alert = UIAlertController(title: "Alert", message: "chon di", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.delegate.passData(with: "xuan trung dep trai")
            //self.dismiss(animated: true, completion: nil)
            self.transferVC1()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func transferVC1(){
        let vc1 = self.storyboard?.instantiateViewController(withIdentifier: "VC1") as! ViewController
        vc1.test_present = "xuan trung hhh"
        self.present(vc1)
    }
}

extension UIViewController {
    
    func present(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.0001
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        present(viewControllerToPresent, animated: false)
    }

}

