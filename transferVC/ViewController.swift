//
//  ViewController.swift
//  transferVC
//
//  Created by Admin on 3/4/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController, ViewController1Delegate {
    
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var label: UILabel!
    var test_present = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        button1.addTarget(self, action: #selector(transferVC), for: .touchUpInside)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        label.text = test_present
    }

    @objc func transferVC(){
        let vc2 = self.storyboard!.instantiateViewController(withIdentifier: "VC2") as! ViewController2
        vc2.delegate = self
        self.present(vc2, animated: true, completion: nil)
        
    }
    func passData(with data: String) {
        //label.text = data
    }
    
    
}


